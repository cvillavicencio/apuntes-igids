/*
Programa: Gestión de aeropuerto.


*/
import java.util.*;

public class Principal {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
	Aeropuerto aero = new Aeropuerto();


	aero.crearAviones();
        boolean salir = false; 
        int opcion; 
 
        while (!salir) {
	    System.out.println("============\n Aeropuerto\n============");
	    System.out.println("1. Listar el ID de todos los aviones.");
	    System.out.println("2. La cantidad de pasajeros actuales en todos los aviones.");
	    System.out.println("3. La cantidad de asientos disponibles en todos los aviones.");
	    System.out.println("4. Buscar y mostrar todos los datos de un avión a través de su ID.");
	    System.out.println("5. Subir pasajeros (recibiendo el ID del avión).");
	    System.out.println("6. Bajar pasajeros (recibiendo el ID del avión).");
	    System.out.println("7. SALIR");
 
            try {
                System.out.println("Escribe una de las opciones");
                opcion = scan.nextInt();
                System.out.println("\n\n");
 
                switch (opcion) {

		case 1:
		    System.out.println("Listar ID: ");
		    aero.listarId();
		    presionaParaContinuar();
		    break;
		case 2:
		    System.out.println("Cantidad de pasajeros en todos los aviones: ");
		    aero.pasajerosABordo();
		    presionaParaContinuar();
		    break;
		case 3:
		    System.out.println("Cantidad de asientos disponibles en todos los aviones: ");
		    aero.asientosDisponibles();
		    presionaParaContinuar();
		    break;
		case 4:
		    System.out.println("Buscar datos de avión por ID.");
		    System.out.println("Ingrese ID de avión: ");
		    int laId = scan.nextInt();
		    aero.datosPorId(laId);
		    presionaParaContinuar();
		    break;
		case 5:
		    System.out.println("Subir pasajeros a un avión");
		    System.out.println("Ingrese ID de avión: ");
		    laId = scan.nextInt();
		    System.out.println("¿Cuántos pasajeros subirán? ");
		    int suben = scan.nextInt();
		    aero.subirPasajeros(laId,suben);
		    presionaParaContinuar();
		    break;

		case 6:
		    System.out.println("Bajar pasajeros a un avión");
		    System.out.println("Ingrese ID de avión: ");
		    laId = scan.nextInt();
		    System.out.println("¿Cuántos pasajeros bajarán? ");
		    int bajan = scan.nextInt();
		    aero.bajarPasajeros(laId,bajan);
		    presionaParaContinuar();
		    break;
		case 7:
		    System.out.println("Hasta pronto!\n\n");
		    salir = true;
		    break;
		default:
		    System.out.println("Esa opción no está disponible.");
                }
            } catch (InputMismatchException e) {
                System.out.println("La opción debe ser un número");
                scan.next();
            }
        }
    }

    // este método es para que el programa sea más cómodo al usar
    // además de esperar que se presione enter para continuar,
    // limpia la pantalla
    public static void presionaParaContinuar(){

	System.out.println("\n\n\n Presiona [ENTER] para continuar...");
	try{
	    System.in.read();
	    // las dos líneas siguientes limpian la pantalla
	    System.out.print("\033[H\033[2J"); 
	    System.out.flush();

	} catch(Exception e) {
	    e.printStackTrace();
	}
	

    }

}


public class Aeropuerto{
    //  Avion[] aviones = new Avion [5]; 
    
    // for (int i = 0; i < this.aviones.length; i++){
    // 	aviones[i]= new Avion(); 
    // }
    Avion avion = new Avion();

    int avionesEnAeropuerto = 100;
    public List<Avion> aviones = new ArrayList<Avion>(avionesEnAeropuerto);
   
    public void crearAviones(){

	for (int i = 0; i < this.avionesEnAeropuerto; i++){
	    Avion elAvion = new Avion();
	    int id = i+1;
	    int pasajerosActual = i*2;
	    int pasajerosMaximo = i*3;
	    boolean estado = true;
	    elAvion.crearAvion(id,
			       pasajerosActual,
			       pasajerosMaximo,
			       estado);
	    aviones.add(elAvion);
	}
    }

    public void listarId(){
	System.out.println("| Id \t| Cap. Maxima\t| Uso actual \t| Asientos Disp.| Estado  \n"+
			   "--------------------------------------------------------------------------");
	for (int i = 0; i < avionesEnAeropuerto ; i++){
	    Avion elAvion = aviones.get(i);
	    System.out.println("| "+elAvion.id+
			       " \t| "+elAvion.pasajerosMaximo+
			       " \t\t| "+elAvion.pasajerosActual+
			       " \t\t| "+elAvion.asientosDisponibles+
			       " \t\t| "+elAvion.estado+" ");
	}
    }

    public void pasajerosABordo(){
	int totalABordo = 0;
	for (int i = 0; i < avionesEnAeropuerto ; i++){
	    Avion elAvion = aviones.get(i);
	    totalABordo += elAvion.pasajerosActual;
	}
	System.out.println("En un total de "+avionesEnAeropuerto+" aviones, hay "+totalABordo+" pasajeros a bordo.");
    }

    public void asientosDisponibles(){
	int totalDisponible = 0;
	for (int i = 0; i < avionesEnAeropuerto ; i++){
	    Avion elAvion = aviones.get(i);
	    int libres = elAvion.pasajerosMaximo - elAvion.pasajerosActual;
	    totalDisponible += libres;
	}
	System.out.println("En un total de "+avionesEnAeropuerto+" aviones, hay "+totalDisponible+" asientos disponibles.");

    }

    public void datosPorId(int id){

       	for (int i=0; i <= avionesEnAeropuerto-1; i++){

	    //	    System.out.println("\ni = "+i+"\nid= "+id);
	    Avion elAvion = aviones.get(i);
	    //System.out.println("\nelAvion.id = "+elAvion.id);
	    
	    if (elAvion.id == id){
	    System.out.println("\n\n\tid:\t\t\t"+elAvion.id+"\n"+
			       "\tCapacidad máxima:\t"+elAvion.pasajerosMaximo+"\n"+
			       "\tUso actual:\t\t"+elAvion.pasajerosActual+"\n"+
			       "\tAsientos disponibles:\t"+elAvion.asientosDisponibles+"\n"+
			       "\tEstado:\t\t\t"+elAvion.estado);
	    return;
	    }
	}	
	System.out.println("No existe ningún avión con la ID proporcionada.");
	return;
    }

    public void subirPasajeros(int id,
			       int suben){
	if (suben < 0){
	    System.out.println("El número de pasajeros que subirán debe ser superior a cero.");
	    return;
	} 

       	for (int i=0; i <= avionesEnAeropuerto-1; i++){
	    Avion elAvion = aviones.get(i);
	    if (elAvion.id == id){
		if(suben <= elAvion.asientosDisponibles){
		    elAvion.pasajerosActual += suben;
		    elAvion.asientosDisponibles -= suben;
		    System.out.println("Han subido "+suben+" pasajeros. Ahora hay "+elAvion.pasajerosActual+" pasajeros a bordo y "+elAvion.asientosDisponibles+" asientos disponibles.");
		    return;
		} else {
		    System.out.println("No pueden subir tantos pasajeros, sólo hay "+elAvion.asientosDisponibles+" asientos disponibles.");
		    return;
		}	   
	    }
	}	
	System.out.println("No existe ningún avión con la ID proporcionada.");
	return;
    }

    public void bajarPasajeros(int id,
			       int bajan){
	if (bajan < 0){
	    System.out.println("El número de pasajeros que subirán debe ser superior a cero.");
	    return;
	} 

       	for (int i=0; i <= avionesEnAeropuerto-1; i++){
	    Avion elAvion = aviones.get(i);
	    if (elAvion.id == id){
		if(bajan <= elAvion.pasajerosActual){
		    elAvion.pasajerosActual -= bajan;
		    elAvion.asientosDisponibles += bajan;
		    System.out.println("Han bajado "+bajan+" pasajeros. Ahora hay "+elAvion.pasajerosActual+" pasajeros a bordo y "+elAvion.asientosDisponibles+" asientos disponibles.");
		    return;
		} else {
		    System.out.println("No pueden bajar tantos pasajeros, sólo hay "+elAvion.pasajerosActual+" pasajeros a bordo.");
		    return;
		}	   
	    }
	}	
	System.out.println("No existe ningún avión con la ID proporcionada.");
	return;
    }
    
}

// 2. Crear clase avión
public class Avion{
    int id = 0;
    int pasajerosMaximo = 41; 
    int pasajerosActual = 32;
    int asientosDisponibles = 9;

    boolean estado = true;

    public void crearAvion(int id,
			   int pasajerosActual,
			   int pasajerosMaximo,
			   boolean estado){

	// 2.a ID valor único correlativo.
	this.id = id;

	// 
	int min = 50;
	int max = 350;  
	this.pasajerosMaximo = (int)(Math.random()*(max-min+1)+min);
	this.pasajerosActual = (int)(Math.random()*(this.pasajerosMaximo-min+1)+min);
	this.asientosDisponibles = this.pasajerosMaximo-this.pasajerosActual;

	Random randbool = new Random();
	this.estado = randbool.nextBoolean();
    }


    // public void getId(){
    // 	return id;
    // }


    // public void getPasajerosMaximo(){
    // 	return this.pasajerosMaximo;
    // }

    // public void getPasajerosActual(){
    // 	return this.pasajerosMaximo;
    // }

    // public void getEstado(){
    // 	return this.estado;
    // }

}

