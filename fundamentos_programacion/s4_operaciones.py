# s4_operaciones.py: Ejercicio de condicionales y operaciones
# Camilo Villavicencio.
#
# 1. En el intérprete de Python, realiza una resta,         L.  9 - 42
# multiplicación y división a partir de estas dos
# variables numéricas:
# (variable 1 = el día de tu cumpleaños | variable 2 = 20)

# día de mi cumpleaños
var1=int(1)
# valor entregado en enunciado
var2=int(20)

print('Las variables disponibles son',var1,' y ',var2,'.\n
Qué operación quieres realizar?\n')

print('  1- restar\n  2- multiplicar\n  3- dividir\n')
operacion = input()

if operacion == '1':
    res1=(var1-var2)
    res2=(var2-var1)

    print("El resultado de",var1,"-",var2,"es: ",res1)
    print("El resultado de",var2,"-",var1,"es: ",res2)

elif operacion == '2':
    res1=(var1*var2)
    res2=(var2*var1)

    print("El resultado de",var1,"*",var2,"es: ",res1)
    print("El resultado de",var2,"*",var1,"es: ",res2)
    
elif operacion == '3':
    res1=(var1/var2)
    res2=(var2/var1)

    print("El resultado de",var1,"/",var2,"es: ",res1)
    print("El resultado de",var2,"/",var1,"es: ",res2)

else:
    print("Ingresaste una opción incorrecta.")
