# s5_promedio.py: Ejercicio de POO
# Camilo Villavicencio.
#
# 1. Se te ha encargado desarrollar un programa en Python   L. 33 - 35
# que calcule el promedio de tres notas ingresadas por
# teclado. Por eso, es importante que el programa permita
# el ingreso de tres notas por teclado.
#
# 2. El programa debe entregar la nota más alta y la nota   L. 38 - 47
# más baja, junto con la nota promedio.
#
# 3. Identifica clase, atributos y métodos a implementar.   L. 22 - 28
#
# 4. Identifica elementos de ingreso y salida de datos.     L. 34 - 37
#
# 5. Identifica operadores y expresiones de cálculo.        L. 41
#
#  Para el desarrollo utilicé la librería NumPy, que aparecía mencionado
# en el apunte correspondiente a la semana 5. Agregué como comentarios el
# código necesario para tener el mismo resultado sin numpy.

import numpy as np
print('\nCálculo de notas\n')

class Notas:
    def preguntar(p):
        print('Ingrese la nota',p)
        return round(float(input()),1)

    def agrupar(a,b,c):
        return np.array([a,b,c])
    
    n1 = 0
    n2 = 0
    n3 = 0

Notas.n1 = (Notas.preguntar('1'))
Notas.n2 = (Notas.preguntar('2'))
Notas.n3 = (Notas.preguntar('3'))
Notas.todas = Notas.agrupar(Notas.n1,Notas.n2,Notas.n3)

promedio  = round(np.mean(Notas.todas),1)
# sin numpy:
#     promedio = round(((Notas.n1+Notas.n2+Notas.n3)/3),1)

enorden   = np.sort(Notas.todas)
notamenor = enorden[0]
notamayor = enorden[2]
# sin numpy: se obtendría el orden de las notas
#     notamenor = min(Notas.n1, Notas.n2, Notas.n3)
#     notamayor = max(Notas.n1, Notas.n2, Notas.n3)

print('\nEl promedio es      :', promedio,
      '\nLa nota más baja es :', notamenor,
      '\nLa nota más alta es :', notamayor)
