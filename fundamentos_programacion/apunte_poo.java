public class Principal {
    public static void main(String[] args) {
	// [A] crea una instancia de la clase Secundaria
	// la instancia se llama sec.
	Secundaria sec = new Secundaria();

	// [B] crea variable que va a incluir como argumento
	// de la instancia algoa, llama al método "chao" e introduce argumento (en este caso, 4).
	sec.main();
	sec.chao(4);

	// [C] crea variable elcolor (que será de tipo String) y le asigna el valor de
	// la variable color que está en la clase Secundaria
	String elcolor = sec.color;
	System.out.println(elcolor);

	// [D] el color (verde por defecto, como se ve en [c]) se cambiará con el método cambiaColor
	// cambiaColor que recibe un argumento de tipo String, "rojo", en este caso.
	sec.cambiaColor("Rojo");


	// [G] esta clase
	sec.claseDesdeTer();
    }    
}


// [a1] la clase Secundaria es llamada en la clase Principal
public class Secundaria { 

    // en este caso, método main no pide argumento.
    public void main(){
	System.out.println("hola "); // solo se muestra este texto
	return;
    }

    // [b1] chao sí pide argumento. (un número entero, al que llamará a)
    public void chao(int a){
	// [b2] acá se concatena "chao " con a, es decir, el argumento que reciba la clase [b1]
	System.out.println("chao "+a); 
	return;
    }

    // [c1] esta variable es llamada por la clase Principal, en [C].
    String color = "verde"; // 


    // [d1] esta clase cambiará el valor de color, este método es llamado en [D]
    public void cambiaColor(String color){
	System.out.println("el color era "+this.color);	
	this.color = color; 
	System.out.println("ahora es "+this.color); // como se aprecia, la asignación que se hace de la variable en
	                                            // la línea anterior se puede utilizar de inmediato
	                        // (por eso es this.color y no color, que es el que está recibiendo como argumento)
    }

    // [e1] en esta clase se creará un objeto desde la clase Terciaria. Ojo que solo es la clase NO la ejecución, 
    // la ejecución ocurre en la clase Principal, en [E].
    public void claseDesdeTer(){
	Ternario ter = new Ternario();
	ter.pregunta(this.color);
    }
}


public class Ternario {

    String teGusta = "Te gusta el color ";

    // [e2] esta clase recibe un argumento. es llamada en [e1]
    public void pregunta (String color){
	this.teGusta += color;
	System.out.println(this.teGusta);
    }
}
